<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

//handles processes and viewing of a page once an endpoint is hit. ex: website.com/endpoint
Route::get('/home', 'HomeController@index')->name('home');
Route::get('/main', 'AccountController@index')->name('accounts.index');
Route::get('/search', 'AccountController@search')->name('search');
