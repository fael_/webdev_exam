<?php

use Illuminate\Database\Seeder;

class AccountSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('accounts')->insert([
            'account_creation_date' => '2020-06-15',
            'account_creation_time' => '17:46',
            'account_updated_date' => '2020-06-15',
            'account_type' => 'SAP Direct',
            'account_owner' => 'Jay-Ar Gumiran',
            'account_manager' => 'Jay-Ar Gumiran',
            'reseller' => 'MSSC',
            'branch' => 'Main',
            'account_name' => 'Minerva Tyreplus Inc.',
            'industry' => 'Automotive',
            'size' => '1-50 Employees',
            'address' => 'Minerva Vineyard, National Road, Putatan, Muntilnupa',
            'contact_person' => 'Mr. Emerson Espeleta',
            'phone_num' => '638620414',
            'email' => 'mseminerva@gmail.com',
            'software_version' => 'SAP Version 9.3',
            'license' => '0 Professional, 0 Limited',
            'asp_coverage_start' => '2020-01-01',
            'asp_coverage_end' => '2021-01-09',
            'asp_status' => '1 Year Support Plan'
        ]);
        DB::table('accounts')->insert([
            'account_creation_date' => '2020-06-15',
            'account_creation_time' => '11:48',
            'account_updated_date' => '2020-06-16',
            'account_type' => 'SAP Direct',
            'account_owner' => 'Ronaldo Baulite',
            'account_manager' => '',
            'reseller' => 'MSSC',
            'branch' => 'Main',
            'account_name' => 'BAYVIEW TECHNOLOGIES INC.',
            'industry' => 'Information Technology',
            'size' => '51-250 Employees',
            'address' => '43rd Floor Yuchengco Tower, RCBC Plaza 6819 Ayala Ave. cor. Gil Puyat Ave. Makati City 1200',
            'contact_person' => 'Mr. Rommel Co',
            'phone_num' => '09176861525',
            'email' => 'rommel.co@bayviewtechnology.com',
            'software_version' => 'SAP Version 9.3',
            'license' => '',
            'asp_coverage_start' => '2020-01-01',
            'asp_coverage_end' => '2020-12-31',
            'asp_status' => '1 Year Support Plan'
        ]);
        DB::table('accounts')->insert([
            'account_creation_date' => '2020-06-10',
            'account_creation_time' => '00:00',
            'account_updated_date' => '2020-06-10',
            'account_type' => 'HRMS',
            'account_owner' => 'Alexandria Agcambert',
            'account_manager' => 'Alexandria Agcambert',
            'reseller' => 'MSSC',
            'branch' => 'Main',
            'account_name' => 'Zvelo Phil Inc.',
            'industry' => 'Information Technology',
            'size' => '1-50 Employees',
            'address' => 'Sedeño, Makati, Metro Manila',
            'contact_person' => 'Ms. Ria Gile',
            'phone_num' => '8567065',
            'email' => 'rgile@zvelo.com',
            'software_version' => 'Payroll Pro',
            'license' => '',
            'asp_coverage_start' => '2019-04-08',
            'asp_coverage_end' => '2020-04-07',
            'asp_status' => '1 Year Support Plan'
        ]);
        DB::table('accounts')->insert([
            'account_creation_date' => '2020-06-04',
            'account_creation_time' => '16:17',
            'account_updated_date' => '2020-06-04',
            'account_type' => 'SAP Direct',
            'account_owner' => 'Roy Jonard De Guzman',
            'account_manager' => 'Racel Tendencia',
            'reseller' => 'MSSC',
            'branch' => 'Cebu',
            'account_name' => 'Organique Inc',
            'industry' => 'Trading & Distribution',
            'size' => '51-250 Employees',
            'address' => 'Unit 1101-1102 Antel Corporate Center 121 Valero Street, Salcedo Village, Makati',
            'contact_person' => 'Rina San Pedro',
            'phone_num' => '09174033020',
            'email' => 'ssanpedro@organique.com.ph',
            'software_version' => 'SAP Version 9.3',
            'license' => '',
            'asp_coverage_start' => '2021-01-01',
            'asp_coverage_end' => '2021-12-31',
            'asp_status' => '1 Year Support Plan'
        ]);
    }
}
