<?php

use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name' => 'Jay-Ar',
            'email' => 'jayar@email.com',
            'password' => Hash::make('password'),
            'role_id' => 1
        ]);
        DB::table('users')->insert([
            'name' => 'Alexandrea',
            'email' => 'alexandria@email.com',
            'password' => Hash::make('password'),
            'role_id' => 1
        ]);
        DB::table('users')->insert([
            'name' => 'Racel',
            'email' => 'racel@email.com',
            'password' => Hash::make('password'),
            'role_id' => 1
        ]);
    }
}
