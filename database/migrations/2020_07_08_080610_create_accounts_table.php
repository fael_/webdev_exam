<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAccountsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('accounts', function (Blueprint $table) {
            $table->id();
            $table->timestamps();
            $table->date('account_creation_date');
            $table->time('account_creation_time');
            $table->date('account_updated_date');
            $table->string('account_type');
            $table->string('account_owner');
            $table->string('account_manager')->nullable();
            $table->string('reseller');
            $table->string('branch');
            $table->string('account_name');
            $table->string('industry');
            $table->string('size');
            $table->string('address');
            $table->string('contact_person');
            $table->bigInteger('phone_num');
            $table->string('email');
            $table->string('software_version');
            $table->string('license')->nullable();
            $table->date('asp_coverage_start');
            $table->date('asp_coverage_end');
            $table->string('asp_status');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('accounts');
    }
}
