@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col">
            {{-- search feature which handles viewing of a single record --}}
            <form action="{{route('search')}}" method="GET" class="search-form mb-3">
                <i class="fa fa-search search-icon"></i>
                <input type="text" name="query" id="query" class="search-box" placeholder="Search by Account Name"> <small> <em>*case sensitive</em></small>
            </form>
        </div>
    </div>
    <div class="row">
        <div class="col">
            {{-- handles viewing of all accounts thru database model connection --}}
            <div class="table-responsive">
                <table class="table table-hover table-striped">
                        <thead class="bg-secondary text-white">
                            <tr>
                                <th scope="col">#</th>
                                <th scope="col">Acct Creation Date</th>
                                <th scope="col">Acct Creation Time</th>
                                <th scope="col">Acct Updated Date</th>
                                <th scope="col">Acct Type</th>
                                <th scope="col">Acct Owner</th>
                                <th scope="col">Acct Manager</th>
                                <th scope="col">Reseller</th>
                                <th scope="col">Branch</th>
                                <th scope="col">Acct Name</th>
                                <th scope="col">Industry</th>
                                <th scope="col">Size</th>
                                <th scope="col">Address</th>
                                <th scope="col">Contact Person</th>
                                <th scope="col">Phone Number</th>
                                <th scope="col">Email Address</th>
                                <th scope="col">Software Version</th>
                                <th scope="col">License</th>
                                <th scope="col">ASP Coverage Start Date</th>
                                <th scope="col">ASP Coverage End Date</th>
                                <th scope="col">ASP Status</th>
                            </tr>
                        </thead>
                        <tbody>
                            {{-- singles out array of data in order to be display singularly --}}
                            @foreach($accounts as $account)
                            <tr>
                                <th scope="row">{{$account->id}}</th>
                                <td>
                                    {{$account->account_creation_date}}
                                </td>
                                <td>
                                    {{$account->account_creation_time}}
                                </td>
                                <td>
                                    {{$account->account_updated_date}}
                                </td>
                                <td>
                                    {{$account->account_type}}
                                </td>
                                <td>
                                    {{$account->account_owner}}
                                </td>
                                <td>
                                    {{$account->account_manager}}
                                </td>
                                <td>
                                    {{$account->reseller}}
                                </td>
                                <td>
                                    {{$account->branch}}
                                </td>
                                <td>
                                    {{$account->account_name}}
                                </td>
                                <td>
                                    {{$account->industry}}
                                </td>
                                <td>
                                    {{$account->size}}
                                </td>
                                <td>
                                    {{$account->address}}
                                </td>
                                <td>
                                    {{$account->contact_person}}
                                </td>
                                <td>
                                    {{$account->phone_num}}
                                </td>
                                <td>
                                    {{$account->email}}
                                </td>
                                <td>
                                    {{$account->software_version}}
                                </td>
                                <td>
                                    {{$account->license}}
                                </td>
                                <td>
                                    {{$account->asp_coverage_start}}
                                </td>
                                <td>
                                    {{$account->asp_coverage_end}}
                                </td>
                                <td>
                                    {{$account->asp_status}}
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
            </div>
        </div>
    </div>
</div>
@endsection