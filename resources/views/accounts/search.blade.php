@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col">
            <a href="{{route('accounts.index')}}" class="btn btn-dark mb-3">Go Back</a>
            <br>
            @foreach ($accounts as $account)
                <h1>{{$account->account_name}}</h1>
                <p><strong>Industry:</strong> {{$account->industry}}</p>
                <p><strong>Size:</strong> {{$account->size}}</p>
                <p><strong>Address:</strong> {{$account->address}}</p>
                <p><strong>Contact Person:</strong> {{$account->contact_person}}</p>
                <p><strong>Email Address:</strong> {{$account->email}}</p>
                <p><strong>Phone Number:</strong> {{$account->phone_num}}</p>
                <p><strong>Account Owner:</strong> {{$account->account_owner}}</p>
                <p><strong>Account Manager:</strong> {{$account->account_manager}}</p>
                <p><strong>Account Type:</strong> {{$account->account_type}}</p>
                <p><strong>Account Creation Date:</strong> {{$account->account_creation_date}}</p>
                <p><strong>Account Creation Time:</strong> {{$account->account_creation_time}}</p>
                <p><strong>Account Updated Date:</strong> {{$account->account_updated_date}}</p>
                <p><strong>Software Version:</strong> {{$account->software_version}}</p>
                <p><strong>License:</strong> {{$account->license}}</p>
                <p><strong>ASP Coverage Start Date:</strong> {{$account->asp_coverage_start}}</p>
                <p><strong>ASP Coverage End Date:</strong> {{$account->asp_coverage_end}}</p>
                <p><strong>ASP Status:</strong> {{$account->asp_status}}</p>
            @endforeach
        </div>
    </div>
</div>
@endsection