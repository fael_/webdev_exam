<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use App\Account;
use Illuminate\Http\Request;
use App\Notifications\SubscriptionDeadline;
use Auth;

class AccountController extends Controller
{
    public function index(Request $request)
    {
        //checks if user is authorized before giving access to webpage and processes
        $this->authorize('viewAny', Account::class);
        $accounts = Account::all();
        $notifications = Auth::user()->notifications;

        //if today's date matches the coverage end date 3 months ahead, then notify user
        foreach($accounts as $account)
        {
            $time = strtotime($account->asp_coverage_end);
            $final = date('Y-m-d', strtotime('-3 month', $time));

            $timeToday = strtotime(date("Y/m/d"));
            $finalToday = date('Y-m-d', strtotime('+3 month', $timeToday));

            $accountName = Account::find($account->id);

            if($final === $finalToday)
            {
                $user = Auth::user();
                $user->notify(new SubscriptionDeadline($accountName));

                return view('accounts.index')->with('accounts',$accounts)->with('notifications', $notifications);
            }
        }

        return view('accounts.index')->with('accounts',$accounts)->with('notifications', $notifications);
    }

    public function search(Request $request)
    {
        $this->authorize('viewAny', Account::class);
        $query = $request->query('query');

        $accounts = Account::all()->whereIn('account_name',$query);

        return view('accounts.search')->with('accounts', $accounts);
    }
}
