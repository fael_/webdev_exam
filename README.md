# Subscription Notification System

A simple cloud-based application designed and built by Earl Rafael Diaz that allows its user to view multiple accounts and get notified whenever an account's subscription date is about to expire.

## Installation
After cloning repository:
```bash
npm install
```

## Features
•Authentication (Login, Register and Logout)

•Search for a single account (Controller function)

•View all accounts (used MySQL database for data storage)

•Responsive Design (thru Bootstrap)

•Notification Button (used MySQL database for data storage and controller function to push data)

•Endpoint Blocking (cannot visit '/main' without logging in)

•3 endpoints ('/home', '/main', '/search')

## Technologies Used
Back-end:
PHP, Laravel, MySQL, REST API, MVC (Model-View-Controller)

Front-end:
Bootstrap, HTML, CSS, Javascript

## Notes
-This is an MVP (Minimum Viable Product) that isn't perfect but has the core functionalities.

-Search keyword has to be specific. ('hello inc' won't work but 'Hello Inc.' will)

-Notifications will add if the current date is similar to the ASP Coverage End date -3 months. I have changed 1 of the accounts to make it similar just to make the notification feature work.

-This was my first time working with notifications which is why I couldn't make it perfect, but I would love to learn to manipulate it better.

-Login credentials can be found at the email I sent to the HR.

## License
[MIT](https://choosealicense.com/licenses/mit/)